package properties;

import org.aeonbits.owner.ConfigFactory;
import properties.interfaces.MainProperties;
import properties.interfaces.UrlProperties;

public class PropertiesManager {

    /**
     * Инициализация класса MainProperties
     *
     * @author Алехнович Александр
     */
    public static UrlProperties urlProperties = ConfigFactory.create(UrlProperties.class);

    /**
     * Инициализация класса UrlProperties
     *
     * @author Алехнович Александр
     */
    public static MainProperties mainProperties = ConfigFactory.create(MainProperties.class);
}
