package properties.interfaces;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({"file:src/main/resources/properties/main.properties",
        "system:properties",
        "system:env"})
public interface MainProperties extends Config {

    /**
     * Метод константы ожидание
     *
     * @author Алехнович Александр
     */
    @Key("timeout")
    int timeout();

    /**
     * Метод константы браузера
     *
     * @author Алехнович Александр
     */
    @Key("browser")
    String browser();

    /**
     * Метод константы разрешения экрана
     *
     * @author Алехнович Александр
     */
    @Key("display_resolution")
    String displayResolution();
}
