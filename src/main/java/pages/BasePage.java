package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.Keys;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selenide.actions;

public abstract class BasePage {

    /**
     * Возвращает список строк элементов страницы
     *
     * @author Алехнович Александр
     */
    public List<String> getElementsStringByXpath(ElementsCollection elementsCollection) {
        List<String> result = new ArrayList<>();
        for (int i = 0; i < elementsCollection.size(); i++) {
            result.add(elementsCollection.get(i).getText());
        }
        return result;
    }

    /**
     * Проходит по страницам с товарами и возвращает количество страниц и список значений названия товара всех страниц
     *
     * @author Алехнович Александр
     */
    public Map<String, List<String>> pageFlipping(SelenideElement selenideElement, ElementsCollection elementsCollection) {
        boolean isShowMore = selenideElement.has(hidden);
        int count = 1;
        Map<String, List<String>> resultsTitlesProductsMap = new LinkedHashMap<>();
        resultsTitlesProductsMap.put(String.valueOf(count), (getElementsStringByXpath(elementsCollection)));
        long startTime = System.currentTimeMillis();
        long waitTime = 2000000;
        long endTime = startTime + waitTime;
        while (!isShowMore && System.currentTimeMillis() < endTime) {
            isShowMore = selenideElement.has(hidden);
            if (!isShowMore) {
                String element = elementsCollection.get(0).getText();
                count++;
                selenideElement.click();
                actions().sendKeys(Keys.CONTROL, Keys.END).build().perform();
                elementsCollection.get(0).shouldHave(not(exactText(element)), Duration.ofSeconds(10));
                resultsTitlesProductsMap.put(String.valueOf(count), (getElementsStringByXpath(elementsCollection)));
            }
        }
        return resultsTitlesProductsMap;
    }
}
