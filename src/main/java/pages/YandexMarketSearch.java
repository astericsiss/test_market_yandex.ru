package pages;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;
import org.apache.commons.lang3.StringUtils;
import org.openqa.selenium.Keys;
import properties.Assertions;

import java.time.Duration;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class YandexMarketSearch extends BasePage {

    /**
     * Список чекбокс в категории "Производитель"
     *
     * @author Алехнович Александр
     */
    private String checkboxName = "//*[contains(@data-zone-data,'Производитель')]//div//span";

    /**
     * Список значений названия товара всех страниц
     *
     * @author Алехнович Александр
     */
    private Map<String, List<String>> resultsTitlesProductsMap = new LinkedHashMap<>();


    /**
     * @author Алехнович Александр
     */
    @Step("Выбираем производителя: {option} ")
    public YandexMarketSearch choiceOfManufacturer(String option) {
        $$x(checkboxName).find(exactText("Показать всё")).click();
        SelenideElement findManufacturer = $x("//*[text()='Найти производителя']//following-sibling::input");
        ElementsCollection resultsProductsTitles = $$x("//*[@data-zone-name='title']");
        findManufacturer.click();
        findManufacturer.sendKeys(option);
        $x(checkboxName).shouldHave(exactText(option)).click();
        resultsProductsTitles.get(0).shouldHave(text(option), Duration.ofSeconds(10));
        actions().sendKeys(Keys.CONTROL, Keys.END).build().perform();
        return this;
    }

    /**
     * @author Алехнович Александр
     */
    @Step("Получаем все предложения товаров")
    public YandexMarketSearch showAllProductPages() {
        SelenideElement showMoreButton = $x("//div[@data-auto='pagination-next']/span");
        ElementsCollection resultsProductsTitles = $$x("//*[@data-zone-name='title']");
        resultsTitlesProductsMap = pageFlipping(showMoreButton, resultsProductsTitles);
        return this;
    }

    /**
     * @author Алехнович Александр
     */
    @Step("Проверяем, что на всех страницах предложения товаров, соответствуют заданному параметру «Производитель»: {option}")
    public YandexMarketSearch validateElementCountProducts(String option) {
        Map<String, List<String>> error = new LinkedHashMap<>();
        for (Map.Entry<String, List<String>> pair : resultsTitlesProductsMap.entrySet()) {
            List<String> product = new ArrayList<>();
            for (String count : pair.getValue()) {
                if (!StringUtils.containsIgnoreCase(count, option)) {
                    product.add(count);
                }
            }
            if (product.size() != 0) {
                error.put("Страница: " + pair.getKey(), product);
            }
        }
        Assertions.assertTrue(error.size() == 0,
                "В предложениях есть товары: " + List.of(error) + " не соответсвующий заданному фильтру производителя: " + option);
        return this;
    }
}
