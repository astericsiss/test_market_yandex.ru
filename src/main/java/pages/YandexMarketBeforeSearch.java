package pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.*;

public class YandexMarketBeforeSearch extends BasePage {

    /**
     * @author Алехнович Александр
     */
    @Step("Переходим на главной странице Яндекс Маркета в: {directory}, {category}, {subCategory}")
    public YandexMarketSearch goToDirectory(String directory, String category, String subCategory) {
        $x("//*[@id='catalogPopupButton']").shouldHave(text(directory)).click();
        ElementsCollection resultCategory = $$x("//*[@id='catalogPopup']//span");
        resultCategory.find(exactText(category)).hover();
        ElementsCollection resultSubCategory = $$x("//*[@role='tabpanel']//a");
        resultSubCategory.find(exactText(subCategory)).hover().click();
        return page(YandexMarketSearch.class);
    }
}
