package pages;

import com.codeborne.selenide.ElementsCollection;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.exactText;
import static com.codeborne.selenide.Selenide.*;

public class YandexBeforeSearch extends BasePage {

    /**
     * @author Алехнович Александр
     */
    @Step("Переходим на Яндекс сервис {service}")
    public <T> T openService(String service, Class<T> typeNextPage) {
        $x("//*[@title='Все сервисы']").click();
        ElementsCollection menu = $$x("//*[@class='services-pinned__popup-content']//div");
        menu.find(exactText(service)).click();
        switchTo().window(1);
        return typeNextPage.cast(page(typeNextPage));
    }
}
