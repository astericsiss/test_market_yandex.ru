package helpers;

import org.junit.jupiter.params.provider.Arguments;

import java.util.stream.Stream;

public class DataProvider {

    /**
     * Данные для тестирования
     *
     * @author Алехнович Александр
     */
    public static Stream<Arguments> provideCheckingProductsList() {
        return Stream.of(
                Arguments.of("Маркет", "Каталог", "Электроника", "Смартфоны", "Apple", "iPhone"),
                Arguments.of("Маркет", "Каталог", "Электроника", "Смартфоны", "ASUS", "ASUS"),
                Arguments.of("Маркет", "Каталог", "Электроника", "Смартфоны", "Black Shark", "Black Shark"),
                Arguments.of("Маркет", "Каталог", "Электроника", "Смартфоны", "OnePlus", "OnePlus"),
                Arguments.of("Маркет", "Каталог", "Электроника", "Смартфоны", "Google", "Google"),
                Arguments.of("Маркет", "Каталог", "Электроника", "Смартфоны", "Seals", "Seals")
        );
    }
}
