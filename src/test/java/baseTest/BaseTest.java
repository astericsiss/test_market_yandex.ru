package baseTest;

import allure.selenide.CustomAllureSelenide;
import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import properties.PropertiesManager;

import static com.codeborne.selenide.Selenide.closeWebDriver;


public class BaseTest {


    /**
     * Перед всеми тестами запускает "Слушатель"
     *
     * @author Алехнович Александр
     */
    @BeforeAll
    public static void setup() {
        SelenideLogger.addListener("AllureSelenide", new CustomAllureSelenide().screenshots(false)
                .savePageSource(false).includeSelenideSteps(false));
    }

    /**
     * Перед каждым тестом открывает браузер с заданным расширением экрана и устанавливает неявные ожидания
     *
     * @author Алехнович Александр
     */
    @BeforeEach
    public void option() {
        Configuration.timeout = PropertiesManager.mainProperties.timeout();
        Configuration.browser = PropertiesManager.mainProperties.browser();
        Configuration.browserSize = PropertiesManager.mainProperties.displayResolution();
    }

    /**
     * После каждого теста закрывает браузер
     *
     * @author Алехнович Александр
     */
    @AfterEach
    public void clear() {
        closeWebDriver();
    }
}
