package ru.yandexMarket;

import baseTest.BaseTest;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import pages.YandexBeforeSearch;
import pages.YandexMarketBeforeSearch;
import properties.PropertiesManager;

import static com.codeborne.selenide.Selenide.open;

public class YandexMarketTests extends BaseTest {

    /**
     * Проверяет, что на всех страницах предложения товаров, соответствуют заданному параметру «Производитель»
     *
     * @author Алехнович Александр
     */

    @DisplayName("Проверка работы яндекс маркета")
    @ParameterizedTest(name = "{displayName}: {arguments}")
    @MethodSource("helpers.DataProvider#provideCheckingProductsList")
    public void yandexMarketTest(String service, String directory, String category, String subCategory,
                                      String optionOne, String optionTwo) {
        open(PropertiesManager.urlProperties.yaUrl(), YandexBeforeSearch.class)
                .openService(service, YandexMarketBeforeSearch.class)
                .goToDirectory(directory, category, subCategory)
                .choiceOfManufacturer(optionOne)
                .showAllProductPages()
                .validateElementCountProducts(optionTwo);
    }
}
